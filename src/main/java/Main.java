import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.PdfPTable;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class Main {
    public static final String DEST = "pdfs/resume.pdf";
    public static final int CELL_NUM = 10;
    public static final int SPACING = 3;
    public static final String[] TEXT =
            {"First name", "Kamil",
                    "Last Name", "Zachara",
                    "Profession", "Stan Culture CEO/student",
                    "Education", "PWSZ Tarnów",
                    "Summary", "A full package, maybe not skilled to the max but committed to the gig"};

    public static void main(String[] args) throws IOException,
            DocumentException {
        File file = new File(DEST);
        new Main().createPdf(DEST);
    }

    public void createPdf(String dest) throws IOException, DocumentException {
        Document document = new Document();
        PdfWriter.getInstance(document, new FileOutputStream(dest));
        document.open();

        Paragraph paragraph = new Paragraph("Resume", new Font(Font.FontFamily.HELVETICA, 25,
                Font.BOLD));
        paragraph.setAlignment(Element.ALIGN_CENTER);
        addEmptyLine(paragraph);
        document.add(paragraph);

        document.add(createTable(TEXT));
        document.close();
    }

    private static void addEmptyLine(Paragraph paragraph) {
        for (int iterator = 0; iterator < Main.SPACING; iterator++) {
            paragraph.add(new Paragraph(" "));
        }
    }

    private static PdfPTable createTable(String[] items) {
        PdfPTable table = new PdfPTable(2);
        for (int index = 0; index < CELL_NUM; index++) {
            PdfPCell cell = new PdfPCell(new Phrase(items[index]));
            cell.setPadding(10);
            table.addCell(cell);
        }
        return table;
    }

}